use std::str::FromStr;
use std::{collections::HashMap, convert::TryFrom, net::IpAddr};

use serde::Deserialize;

#[derive(Deserialize)]
pub struct InnerLogin {
    pub check_pwd: String,
    pub check_session: String,
    pub check_user: String,
    pub login_confirm: String,
}

#[derive(Debug)]
pub struct EthStatus {
    pub link: bool,
    pub media_type: u16,
}

#[derive(Debug)]
pub struct TxRx<T> {
    pub tx: T,
    pub rx: T,
}

#[derive(Debug)]
pub struct WiFiStatus {
    pub enabled: bool,
    pub security: String,
    pub ssid: String,
}

#[derive(Debug)]
pub struct Diagnostics {
    pub eth_status: Vec<EthStatus>,
    pub lan_ip: IpAddr,
    pub line_rate: TxRx<u32>,
    pub dhcp_server: bool,
    pub usb_ports: Vec<String>,
    pub wan_link: bool,
    pub wan_model: String,
    pub wan_gateway: IpAddr,
    pub wan_ip: IpAddr,
    pub wifi_status: Vec<WiFiStatus>,
}

impl TryFrom<HashMap<String, HashMap<String, String>>> for Diagnostics {
    type Error = super::Error;
    fn try_from(hm: HashMap<String, HashMap<String, String>>) -> Result<Self, Self::Error> {
        let inner_hm = hm.get("diagnostic").ok_or(Self::Error::ParseError)?;
        let numbered_keys = ["eth", "wl", "usb_port"]
            .iter()
            .map(|x| {
                (
                    x,
                    inner_hm
                        .keys()
                        .filter(|y| y.starts_with(x))
                        .map(|y| {
                            y.replace(x, "")
                                .split('_')
                                .next()
                                .unwrap()
                                .parse::<usize>()
                                .unwrap()
                        })
                        .max()
                        .unwrap(),
                )
            })
            .collect::<HashMap<_, _>>();
        let eth_status =
            (0..*(numbered_keys.get(&"eth").unwrap())).try_fold(vec![], |mut acc, x| {
                let es = EthStatus {
                    link: inner_hm
                        .get(&format!("eth{}_link", x + 1))
                        .ok_or(Self::Error::ParseError)?
                        == "1",
                    media_type: inner_hm
                        .get(&format!("eth{}_media_type", x + 1))
                        .ok_or(Self::Error::ParseError)?
                        .parse::<u16>()
                        .map_err(|_| Self::Error::ParseError)?,
                };
                acc.push(es);
                Ok(acc)
            })?;
        let wifi_status =
            (0..*(numbered_keys.get(&"wl").unwrap())).try_fold(vec![], |mut acc, x| {
                let es = WiFiStatus {
                    enabled: inner_hm
                        .get(&format!("wl{}_enabled", x + 1))
                        .ok_or(Self::Error::ParseError)?
                        == "1",
                    security: inner_hm
                        .get(&format!("wl{}_security", x + 1))
                        .ok_or(Self::Error::ParseError)?
                        .clone(),
                    ssid: inner_hm
                        .get(&format!("wl{}_ssid", x + 1))
                        .ok_or(Self::Error::ParseError)?
                        .clone(),
                };
                acc.push(es);
                Ok(acc)
            })?;
        Ok(Self {
            eth_status,
            lan_ip: IpAddr::from_str(inner_hm.get("lanip").ok_or(Self::Error::ParseError)?)
                .map_err(|_| Self::Error::ParseError)?,
            line_rate: TxRx {
                tx: inner_hm
                    .get("linerate_us")
                    .ok_or(Self::Error::ParseError)?
                    .parse::<u32>()
                    .map_err(|_| Self::Error::ParseError)?,
                rx: inner_hm
                    .get("linerate_ds")
                    .ok_or(Self::Error::ParseError)?
                    .parse::<u32>()
                    .map_err(|_| Self::Error::ParseError)?,
            },
            dhcp_server: inner_hm.get("udhcpd").ok_or(Self::Error::ParseError)? == "1",
            usb_ports: (0..*(numbered_keys.get(&"usb_port").unwrap())).try_fold(
                vec![],
                |mut acc, x| {
                    acc.push(
                        inner_hm
                            .get(&format!("usb_port{}", x + 1))
                            .ok_or(Self::Error::ParseError)?
                            .clone(),
                    );
                    Ok(acc)
                },
            )?,
            wan_link: inner_hm.get("wan_link").ok_or(Self::Error::ParseError)? == "1",
            wan_model: inner_hm
                .get("wan_model")
                .ok_or(Self::Error::ParseError)?
                .clone(),
            wan_gateway: IpAddr::from_str(inner_hm.get("wangw").ok_or(Self::Error::ParseError)?)
                .map_err(|_| Self::Error::ParseError)?,
            wan_ip: IpAddr::from_str(inner_hm.get("wanip").ok_or(Self::Error::ParseError)?)
                .map_err(|_| Self::Error::ParseError)?,
            wifi_status,
        })
    }
}
