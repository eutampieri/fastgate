use std::{collections::HashMap, convert::TryFrom};
use types::*;

mod types;

fn get_time() -> String {
    format!(
        "{}",
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .map(|x| x.as_millis())
            .unwrap_or(0)
    )
}

#[derive(Debug)]
pub enum Error {
    HttpError,
    WrongCredentials,
    ParseError,
    Unauthenticated,
}

pub struct Fastgate {
    agent: ureq::Agent,
    logged_in: bool,
    ip: std::net::IpAddr,
    xsrf_token: Option<String>,
}

impl Fastgate {
    fn get_cookie(&self, name: &str) -> Result<String, Error> {
        Ok(self
            .agent
            .cookie_store()
            .get(&format!("{}", self.ip), "/", dbg!(name))
            .ok_or(Error::Unauthenticated)?
            .value()
            .to_owned())
    }
    pub fn new(ip: std::net::IpAddr) -> Self {
        Self {
            agent: ureq::agent(),
            logged_in: false,
            ip,
            xsrf_token: None,
        }
    }

    fn send_credentials(
        &mut self,
        username: &str,
        password: &str,
    ) -> Result<ureq::Response, Error> {
        self.agent
            .post(&format!("http://{}/status.cgi", self.ip))
            .set("X-XSRF-TOKEN", &get_time())
            .send_form(&[
                ("cmd", "3"),
                ("username", username),
                ("password", password),
                ("remember_me", "1"),
                ("act", "nvset"),
                ("service", "login_confirm"),
                ("sessionKey", "null"),
                ("_", &get_time()),
            ])
            .map_err(|_| Error::HttpError)
    }

    pub fn login(&mut self, username: &str, password: &str) -> Result<(), Error> {
        dbg!(self.send_credentials(username, password)?.all("Set-Cokie"));
        let response = self.send_credentials(username, password)?;
        dbg!(response.all("Set-Cookie"));
        let parsed_response: HashMap<String, InnerLogin> =
            response.into_json().map_err(|_| Error::ParseError)?;
        self.xsrf_token = self.get_cookie("XSRF-TOKEN").ok();
        let login_details = parsed_response
            .get("login_confirm")
            .ok_or(Error::ParseError)?;
        if login_details.check_pwd != "1" || login_details.check_user != "1" {
            Err(Error::WrongCredentials)
        } else {
            self.logged_in = true;
            Ok(())
        }
    }

    pub fn iface_stats(&mut self) -> Result<Diagnostics, Error> {
        let session_id = self.get_cookie("SID")?;
        let xsrf_token = self
            .xsrf_token
            .as_ref()
            .map(|x| x.clone())
            .ok_or(Error::Unauthenticated)?;
        let raw_resp: HashMap<String, HashMap<String, String>> = self
            .agent
            .get(&format!(
                "http://{}/status.cgi?_={}&nvget=diagnostics&sessionKey={}",
                self.ip,
                get_time(),
                session_id,
            ))
            .set("X-XSRF-TOKEN", &xsrf_token)
            .call()
            .map_err(|_| Error::HttpError)?
            .into_json()
            .map_err(|_| Error::ParseError)?;
        Diagnostics::try_from(raw_resp)
    }
}

#[cfg(test)]
mod tests {
    use std::net::{IpAddr, Ipv4Addr};

    use super::*;
    #[test]
    fn it_works() {
        let mut fg = Fastgate::new(IpAddr::V4(Ipv4Addr::new(10, 168, 1, 254)));
        fg.login("admin", "admin").unwrap();
        println!("{:?}", fg.iface_stats().unwrap());
    }
}
